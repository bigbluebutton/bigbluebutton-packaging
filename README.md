BigBlueButton Packaging from Sources
====================================

The purpose of the repo is to create BigBlueButton packages from a git repository.
This includes the possibility to create packages which include custom patches.
Packages are built within an Ubuntu 16.04 docker container.

Upstream source for packagin is https://gitlab.hrz.tu-chemnitz.de/bigbluebutton/bigbluebutton-packaging

Differences from the original BBB Packages
------------------------------------------
* packages contain the BBB Version number in the package name
* postinst/postrm/preinst/prerm scripts are from original packages

Build instructions
------------------

* clone this repository
    ~~~bash
    git clone https://gitlab.hrz.tu-chemnitz.de/bigbluebutton/bigbluebutton-packaging.git
    cd bigbluebutton-packaging
    ~~~
* build a docker container for building BBB
    ~~~bash
    docker build -t bbb-build .
    ~~~
* run the containter for building
    ~~~bash
    docker run --rm -v /path/to/deb/output/dir:/root/build -v /path/to/this/repo/clone:/root/build/bigbluebutton-packaging bbb-build
    ~~~
* wait for the packages to appear in `/path/to/deb/output/dir`

Customizations
--------------

You can build from your own BBB repo and a custom branch:

* add an entry to `debian/changelog`. It should contain the short commit id in the version number which you want to build
* run the container again


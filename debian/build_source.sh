#!/bin/bash
GIT_TAG="$1"
BUILDROOT="$2"

export SDKMAN_DIR="/root/.sdkman"
[[ -s "/root/.sdkman/bin/sdkman-init.sh" ]] && source "/root/.sdkman/bin/sdkman-init.sh"
which grails
which sbt

#echo "Building $GIT_TAG and install it to ${BUILDROOT}"
tmpdir="$(mktemp -d)"
git clone https://gitlab.hrz.tu-chemnitz.de/bigbluebutton/bigbluebutton.git "${tmpdir}/bigbluebutton"
cd "${tmpdir}/bigbluebutton"
git checkout "${GIT_TAG}"

# Meteor Frontend
cd bigbluebutton-html5
meteor update --allow-superuser --release 1.8 > /tmp/build.out
meteor npm install --production --allow-superuser >> /tmp/build.out
mkdir ${tmpdir}/meteorbundle
meteor build --allow-superuser --server-only ${tmpdir}/meteorbundle >> /tmp/build.out
echo "Meteor gebaut in ${tmpdir}/meteorbundle"
#ls ${tmpdir}/meteorbundle
echo mkdir -p "${BUILDROOT}"/usr/share/meteor
mkdir -p "${BUILDROOT}"/usr/share/meteor
echo "foo" > "${BUILDROOT}"/usr/share/meteor/foo
tar -xf ${tmpdir}/meteorbundle/bigbluebutton-html5.tar.gz -C "${BUILDROOT}"/usr/share/meteor --no-same-owner
cd "${BUILDROOT}"/usr/share/meteor/bundle/programs/server
PATH="${tmpdir}/bigbluebutton/bigbluebutton-html5/.meteor/local/dev_bundle/bin:$PATH" npm install

## Scala Apps
mkdir -p ~/.sbt/1.0/
cat > ~/.sbt/1.0/global.sbt <<END
resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
updateOptions := updateOptions.value.withCachedResolution(true)
END
cd "${tmpdir}/bigbluebutton/bbb-common-message"
./deploy.sh >> /tmp/build.out
cd "${tmpdir}/bigbluebutton/bbb-common-web"
./deploy.sh >> /tmp/build.out

cd "${tmpdir}/bigbluebutton/bigbluebutton-web/"
./build.sh >> /tmp/build.out
grails assemble >> /tmp/build.out
mkdir -p "${BUILDROOT}"/usr/share/bbb-web
jar xvf "${tmpdir}/bigbluebutton/bigbluebutton-web/build/libs/bigbluebutton-0.10.0.war" -C "${BUILDROOT}"/usr/share/bbb-web .

cd "${tmpdir}/bigbluebutton/akka-bbb-apps"
sbt clean stage
echo mkdir -p "${BUILDROOT}"/usr/share/bbb-apps-akka
mkdir -p "${BUILDROOT}"/usr/share/bbb-apps-akka
echo foo > "${BUILDROOT}"/usr/share/bbb-apps-akka/foo
cp -a target/universal/stage/* "${BUILDROOT}"/usr/share/bbb-apps-akka

cd "${tmpdir}/bigbluebutton/bbb-fsesl-client"
sbt clean publish publishLocal
cd "${tmpdir}/bigbluebutton/akka-bbb-fsesl"
sbt clean stage
echo mkdir -p "${BUILDROOT}"/usr/share/bbb-fsesl-akka
mkdir -p "${BUILDROOT}"/usr/share/bbb-fsesl-akka
echo foo > "${BUILDROOT}"/usr/share/bbb-fsesl-akka/foo
cp -a target/universal/stage/* "${BUILDROOT}"/usr/share/bbb-fsesl-akka

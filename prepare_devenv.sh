#!/usr/bin/env bash
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"

sdk install gradle 5.5.1
sdk install grails 3.3.9
sdk install sbt 1.2.8
sdk install maven 3.5.0

curl https://install.meteor.com/?release=1.8.1 | sed s/--progress-bar/-sL/g | sh

mkdir -p /root/.sbt/1.0
cat > /root/.sbt/1.0/global.sbt <<END
resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
updateOptions := updateOptions.value.withCachedResolution(true)
END


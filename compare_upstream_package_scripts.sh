#!/usr/bin/env bash

upstream_dir="$1"
if [[ -z $upstream_dir ]] ; then
    echo "Script braucht Pfad zum Clone von https://github.com/alangecker/bbb-packages.git als Argument"
    echo "Aufruf mit $0 ../bbb-packages"
    exit
fi

script_dir="$(realpath $(dirname $0))"

echo "vergleiche Paketscripte"
for package in bbb-html5 bbb-apps-akka; do 
    for script in postinst postrm preinst prerm; do 
        diff -u "${script_dir}"/debian/${package}.${script} "${upstream_dir}"/${package}/control/${script} 
    done
done

for package in bbb-html5 bbb-apps-akka; do
    grep "^${package}/" "${script_dir}/debian/${package}.install" | while read file target; do
        diff -u "${script_dir}/$file" "${upstream_dir}/${package}/data/${target}" 
    done
done

FROM ubuntu:16.04
RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install git-core ant ant-contrib openjdk-8-jdk-headless devscripts zip debhelper
ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
COPY prepare_devenv.sh .
RUN bash prepare_devenv.sh
COPY build.sh .
CMD /bin/bash -l /build.sh
